package az.ingress.ms6demo.controller;

import az.ingress.ms6demo.dto.CreateStudentDto;
import az.ingress.ms6demo.dto.StudentDto;
import az.ingress.ms6demo.exceptions.StudentNotFound;
import az.ingress.ms6demo.services.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/students")
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    public ResponseEntity<StudentDto> get(@PathVariable Long id) {
        log.trace("Get student by id {}", id);
        try {
            return ResponseEntity.ok(studentService.getStudent(id));
        } catch (StudentNotFound studentNotFound) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<StudentDto> create(@RequestBody @Valid CreateStudentDto dto) {
        log.trace("Create student body {}", dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.createStudent(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentDto> update(@PathVariable Long id, @RequestBody @Valid CreateStudentDto dto) {
        log.trace("Update student by id {} body {}", id, dto);
        try {
            return ResponseEntity.ok(studentService.updateStudent(id, dto));
        } catch (StudentNotFound studentNotFound) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        log.trace("Delete student by id {}", id);
        try {
            studentService.deleteStudent(id);
            return ResponseEntity.noContent().build();
        } catch (StudentNotFound studentNotFound) {
            return ResponseEntity.notFound().build();
        }
    }


}
