package az.ingress.ms6demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms6demoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms6demoApplication.class, args);
	}

}
